/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjercicioA;

/**
 *
 * @author alder
 */
public class Rectangulo {
       private int x1,x2,y1,y2;
       
       public Rectangulo(int x1,int x2,int y1, int y2){
           this.x1=x1;
           this.x2=x2;
           this.y1=y1;
           this.y2=y2;         
           
       }
       
       public static void mostrarCoordenadas(Rectangulo figura){
           System.out.println("Las coordenadas son: " + figura.x1 + ", " + figura.x2 + ", " + figura.y1 + ", " + figura.y2);
       }
       
       public static void setCoordenadas(Rectangulo figura, int x1,int x2,int y1, int y2){
           figura.x1=x1;
           figura.x2=x2;
           figura.y1=y1;
           figura.y2=y2;
       }
          
       
       
       public static int mostrarPerimetro(Rectangulo figura){
           
           int perimetro;
                
                perimetro=((figura.y1-figura.x1)*2)+((figura.y2-figura.x2)*2);
           
           return perimetro;
       }
       
       public static int mostrarArea(Rectangulo figura){
           int area;
            
                area=(figura.y1-figura.x1)*(figura.y2-figura.x2);
            
           return area;
       }
}
