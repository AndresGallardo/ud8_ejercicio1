/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjercicioA;
import java.util.Scanner;
/**
 *
 * @author alder
 */
public class UD8_ProgramaPunto {
public static void matrizPersonas(Persona[] arrayPersonas){
    Scanner teclado = new Scanner(System.in);        

        String DNI;
        String nombre;
        String apellidos;
        int edad;
                
        
        for(int i=0;i<arrayPersonas.length;i++){
            System.out.println("");
            System.out.println("Por favor, introduzca el DNI de la persona" + (i+1));
            DNI=teclado.next();
            System.out.println("Por favor, introduzca el nombre de la persona" + (i+1));
            nombre=teclado.next();
            System.out.println("Por favor, introduzca los apellidos de la persona" + (i+1));
            apellidos=teclado.nextLine();
            apellidos=teclado.nextLine();
            System.out.println("Por favor, introduzca la edad de la persona" + (i+1));
            edad=teclado.nextInt();
            
            arrayPersonas[i]= new Persona(DNI,nombre,apellidos,edad);
        }  
    }
    
    public static void imprimePersonas(Persona[] arrayPersonas){
        
        for(int i=0;i<arrayPersonas.length;i++){
                        System.out.println("Los datos de la persona " + (i+1) + " son: " );
                        System.out.println("DNI: " + arrayPersonas[i].DNI);
                        System.out.println("Nombre: " + arrayPersonas[i].nombre);
                        System.out.println("Apellidos: " + arrayPersonas[i].apellidos);      
                        System.out.println("Edad: " + arrayPersonas[i].edad);
                        
                        if(arrayPersonas[i].mayor==true){
                            System.out.println("Además, esta persona es mayor de edad");
                        }else{
                            System.out.println("Además, esta persona NO es mayor de edad");
                        }
                        System.out.println("");
        } 
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Punto coordenada1=new Punto(5,0);
        Punto coordenada2=new Punto(10,10);
        Punto coordenada3=new Punto(-3,7);     
        
        
        
        Scanner teclado = new Scanner(System.in);
        
        int cantidad;
        
        
        System.out.println("Por favor, indique el número de personas que va a introducir");
        cantidad=teclado.nextInt();
        
        Persona[] arrayPersonas=new Persona[cantidad];
        
        matrizPersonas(arrayPersonas);
        
        imprimePersonas(arrayPersonas);
            
          
        
        
   }
    
    
}
